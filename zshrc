 #to your oh-my-zsh installation.
 source ~/Dependencias-doge/zsh-autocomplete/zsh-autocomplete.plugin.zsh

fpath+=$HOME/.zsh/typewritten
autoload -U promptinit; promptinit
prompt typewritten

export ZSH=$HOME/.oh-my-zsh


# Case-sensitive completion must be off. _ and - will be interchangeable.
# HYPHEN_INSENSITIVE="true"

# Uncomment the following line to disable bi-weekly auto-update checks.
ZSH_THEME="" #spaceship
DISABLE_AUTO_UPDATE="true"
DISABLE_AUTO_PROMP="true"

# Which plugins would you like to load?
# Standard plugins can be found in $ZSH/plugins/
# Custom plugins may be added to $ZSH_CUSTOM/plugins/
# Example format: plugins=(rails git textmate ruby lighthouse)
# Add wisely, as too many plugins slow down shell startup.
plugins=(git
        vi-mode
        #zsh-autosuggestions
        zsh-syntax-highlighting)

source $ZSH/oh-my-zsh.sh


# On-demand rehash
zshcache_time="$(date +%s%N)"

autoload -Uz add-zsh-hook

rehash_precmd() {
  if [[ -a /var/cache/zsh/pacman ]]; then
    local paccache_time="$(date -r /var/cache/zsh/pacman +%s%N)"
    if (( zshcache_time < paccache_time )); then
      rehash
      zshcache_time="$paccache_time"
    fi
  fi
}

add-zsh-hook -Uz precmd rehash_precmd

# omz
alias zshconfig="geany ~/.zshrc"
alias ohmyzsh="thunar ~/.oh-my-zsh"

# ls
alias l='exa --long --header --inode --git --group-directories-first'
alias lt='exa --long --header --inode --git --group-directories-first --level=2 --icons'
alias ll='ls -lah'
alias la='ls -A'
alias lm='ls -m'
alias lr='ls -R'
alias lg='lazy git'

# git
alias gcl='git clone --depth 1'
alias gi='git init'
alias ga='git add'
alias gc='git commit -m'
alias gp='git push origin master'
alias gcache='git rm cache .'

# personal
alias vcache='rm -rf ~/.config/nvim ; rm -rf ~/.local/share/nvim ; rm -rf ~/.cache/nvim'
alias op='thunar'
alias q='cd ; exit'
alias hack='nvim ~/.zshrc'
alias fresh='source ~/.zshrc'
alias data='cd ; /opt/dataspell-2022.1.1/bin/ ; sh dataspell.sh'
alias dpress='bash $HOME/Desarrollo/github-repos/dpress/dpress.sh'
alias dk='docker'
alias dc='docker-compose'
alias c='clear'
alias py='python'
alias dp='doppler'

# create and cd
mk(){
    mkdir $1 ; cd $1
}

# execute ranger or not regarding if I put an arguement or not
dev(){
  if [[ $# -eq 1 ]]; then
    ~/Desarrollo/$1
  else
    ~/Desarrollo/ ; ranger
  fi
}

# vim mode
bindkey -v

# nvim
export NVM_DIR="$HOME/.nvm"
[ -s "$NVM_DIR/nvm.sh" ] && \. "$NVM_DIR/nvm.sh"  # This loads nvm
[ -s "$NVM_DIR/bash_completion" ] && \. "$NVM_DIR/bash_completion"  # This loads nvm bash_completion

# >>> conda initialize >>>
# !! Contents within this block are managed by 'conda init' !!
__conda_setup="$('/home/standoge/miniconda3/bin/conda' 'shell.zsh' 'hook' 2> /dev/null)"
if [ $? -eq 0 ]; then
    eval "$__conda_setup"
else
    if [ -f "/home/standoge/miniconda3/etc/profile.d/conda.sh" ]; then
        . "/home/standoge/miniconda3/etc/profile.d/conda.sh"
    else
        export PATH="/home/standoge/miniconda3/bin:$PATH"
    fi
fi
unset __conda_setup
# <<< conda initialize <<<


# pnpm
export PNPM_HOME="/home/standoge/.local/share/pnpm"
export PATH="$PNPM_HOME:$PATH"
# pnpm end
export VOLTA_HOME="$HOME/.volta"
export PATH="$VOLTA_HOME/bin:$PATH"
